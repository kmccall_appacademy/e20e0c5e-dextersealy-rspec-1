def pig_prefix(word)
  prefix = ""
  0.upto(word.length) do |i|
    break if word[i].match(/[aeiouAEIOU]/) && (i == 0 || word[i-1,2] != "qu")
    prefix << word[i]
  end
  prefix
end

def translate_word(word)
  prefix = pig_prefix(word)
  word[prefix.length, word.length] + prefix + "ay"
end

def translate(string)
  result = []
  string.split.each do |word|
    result << translate_word(word)
  end

  result.join(" ")
end
