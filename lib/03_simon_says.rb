def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, count=2)
  strings = []
  count.times do
    strings << string
  end

  strings.join(" ")
end

def start_of_word(word, count)
  word[0,count]
end

def first_word(string)
  string.split()[0]
end

def titleize(string)
  result = []
  string.split().each do |word|
    if result.length == 0 || !['and', 'over', 'the'].include?(word)
      result << word.capitalize
    else
      result << word
    end
  end
  result.join(" ")
end
