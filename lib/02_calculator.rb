def add(first_number, second_number)
  first_number + second_number
end

def subtract(left_hand_number, right_hand_number)
  left_hand_number - right_hand_number
end

def sum(numbers)
  numbers.inject(0, :+)
end

def multiply(numbers)
  numbers.inject(1, :*)
end

def power(base, exponent)
    base ** exponent
end

def factorial(number)
    (1..number).reduce(1) { |factorial, i| factorial *= i }
end
